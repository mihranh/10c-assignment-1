#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <ctime>
#include <cstdlib>
#include <iomanip>
#include "cards.h"
using namespace std;

int main(){
	Player you(100);
	Hand yours;
	Hand dealers;
	Player dealer(900);
	string response;
	int bet;
	int gamenumber = 0;

	srand(static_cast<int>(time(0)));

	ofstream fout;
	fout.open("gamelog.txt");

	do {
		cout << "You have $" << you.get_money() << ". Enter bet: " << endl;

		// input validation
		while (!(cin >> bet) || bet > you.get_money() || bet <= 0) {
			cout << "Try again. Enter bet:";
		}

		// Player's Turn
		cout << "Your cards:" << endl;
		yours.draw_card();
		yours.print_hand_console();
		cout << "Your total is " << yours.sum_hand() << ". Do you want another card (y/n)?";
		cin >> response;

		while ((response.substr(0, 1) == "Y" || response.substr(0, 1) == "y") && yours.sum_hand() < 7.5) {
			cout << "\n\nNew Card:" << endl;
			yours.draw_card();
			yours.print_draw_console();
			yours.sort_hand();

			cout << "Your cards:\n";
			yours.print_hand_console();
			cout << "Your total is " << yours.sum_hand() << ". ";

			if (yours.sum_hand() < 7.5) {
				cout << "Do you want another card(y / n) ? ";
				cin >> response;
			}
			else
				response = "no";
		}

		//Dealer's Turn
		cout << "\n\n\n\nDealer's cards:" << endl;
		dealers.draw_card();
		dealers.print_hand_console();
		cout << "Dealer's total is " << dealers.sum_hand() << ".\n\n\n";

		while (dealers.sum_hand() < 5.5) {
			cout << "New Card:" << endl;
			dealers.draw_card();
			dealers.print_draw_console();
			dealers.sort_hand();

			cout << "Dealer's cards:\n";
			dealers.print_hand_console();
			cout << "Dealer's total is " << dealers.sum_hand() << ".\n\n\n";
		}

		// Prints game record to gamelog.txt
		gamenumber++;
		fout << "-------------------------------------------------------------\n\n"
			<< "Game Number: " << gamenumber
			<< setw(20) << " Money Left: " << you.get_money() << "\n"
			<< "Bet: " << bet << "\n\n";

		fout << "Your Cards:\n";
		yours.print_hand_file(fout);
		fout << "Your total is " << yours.sum_hand() << ".\n\n";

		fout << "Dealer's Cards:\n";
		dealers.print_hand_file(fout);
		fout << "Dealer's total is " << dealers.sum_hand() << ".\n\n";

		// Determines winner and updates money
		if (yours.sum_hand() >= 7.5) {
			you.change_money(-bet);
			cout << "Too bad. You lose " << bet << ".\n";
		}
		else if (yours.sum_hand() <= 7.5 && dealers.sum_hand() <= 7.5 && yours.sum_hand() > dealers.sum_hand()) {
			you.change_money(bet);
			dealer.change_money(-bet);
			cout << "You Win " << bet << "!\n";
		}
		else if (yours.sum_hand() <= 7.5 && dealers.sum_hand() <= 7.5 && yours.sum_hand() <= dealers.sum_hand()) {
			you.change_money(-bet);
			cout << "Too bad. You lose " << bet << ".\n";
		}
		else if (yours.sum_hand() <= 7.5 && dealers.sum_hand() > 7.5) {
			you.change_money(bet);
			dealer.change_money(-bet);
			cout << "You Win " << bet << "!\n";
		}
		else
			you.change_money(0);

		// Resets yours and dealer's hands
		yours.reset_hand();
		dealers.reset_hand();

	} while (you.get_money() > 0 && dealer.get_money() > 0);

	if (you.get_money() <= 0){
		cout << "GET OUT OF MY CASINO!\n";
	}
	else if (dealer.get_money() <= 0){
		cout << "Congratulations, You have beat the Dealer!\n";
	}

	fout.close();
	return 0;
}